.. django-eveuniverse documentation master file, created by
   sphinx-quickstart on Sun Aug  9 16:09:32 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.md
   :parser: myst_parser.sphinx_

.. toctree::
   :hidden:
   :maxdepth: 3

   api
   settings
